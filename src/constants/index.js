import {
  mobile,
  backend,
  creator,
  web,
  javascript,
  typescript,
  html,
  css,
  reactjs,
  redux,
  tailwind,
  nodejs,
  mongodb,
  git,
  figma,
  docker,
  meta,
  starbucks,
  tesla,
  shopify,
  carrent,
  jobit,
  tripguide,
  threejs,
  python,
  postgres,
  redis,
  django,
  bash,
  jira,
  netronome,
  uct,
  mira,
  km,
  bo,
  ryan,
  nolan,
  rando,
  jt,
  sammy,
  boje,
  charl,
  eit,
  taskit,
  invest,
  shyft,
  drone,
} from "../assets";

export const navLinks = [
  {
    id: "about",
    title: "About",
  },
  {
    id: "work",
    title: "Work",
  },
  {
    id: "contact",
    title: "Contact",
  },
];

const services = [
  {
    title: "Web Development",
    icon: web,
  },
  {
    title: "Backend Development",
    icon: backend,
  },
  {
    title: "Machine Learning",
    icon: creator,
  },
  {
    title: "Research and Development",
    icon: mobile,
  },
];

const technologies = [
  {
    name: "Python",
    icon: python,
  },
  {
    name: "PostgreSQL",
    icon: postgres,
  },
  {
    name: "MongoDB",
    icon: mongodb,
  },
  {
    name: "Redis",
    icon: redis,
  },
  {
    name: "Django",
    icon: django,
  },
  {
    name: "TypeScript",
    icon: typescript,
  },
  {
    name: "Node JS",
    icon: nodejs,
  },
  {
    name: "git",
    icon: git,
  },
  {
    name: "figma",
    icon: figma,
  },
  {
    name: "docker",
    icon: docker,
  },
  {
    name: "Bash",
    icon: bash,
  },
  {
    name: "Jira",
    icon: jira,
  },  
];

const experiences = [
  {
    title: "Embedded Network Software Engineer",
    company_name: "Netronome",
    icon: netronome,
    iconBg: "#004da3",
    date: "Feb 2018 - Nov 2021",
    points: [
      "Comprehensive testing of firmware and driver components.",
      "Architecting cloud deployment scenarios and developing test plans, using NFVI and MANO technologies.",
      "Mentoring junior engineers.",
      "Sales support: Plan and execute proof-of-concept investigations into innovative product use-cases.",
      "Issue tracking.",
      "Conducting code reviews and providing constructive feedback to other developers.",
      "Implementating, testing and enhancement of load-balancers, DDoS and IPS eBPF XDP products.",
      "Kernel TLS encryption network card offload validation.",
      "Designing, developing and maintaining network secure transactions on highly distributed data-intensive applications.",
      "Lead developer of an OpenStack automation framework.",
      "Developing and maintaining eBPF and XDP kernel-based applications using the C programming language and other related technologies.",
      "Collaborating with cross-functional teams including designers, product managers, and other developers to create high-quality products.",
    ],
  },
  {
    title: "Senior Researcher",
    company_name: "University of Cape Town",
    icon: uct,
    iconBg: "#E6DEDD",
    date: "Aug 2018 - Feb 2021",
    points: [
      "Supervised postgraduates and final-year undergraduates. (Key areas: Leadership and mentoring)",
      "Research activities related to computational intelligence, biomedical science and engineering. (Key areas: Orthogonalization, inverse problems, control systems, tomography, instrumentation, signals and systems, tissue properties, Newton methods, and anomaly detection)",
      "Reviewed scientific papers for the Institute of Electrical and Electronics Engineers (IEEE).",
      "Co-supervising and managing the Control and Robotics Engineering laboratory. (Key areas: Leadership and organization)",
      "Developing high-performance image reconstruction software. (Key areas: Independent thinker and problem solver)",
    ],
  },
  {
    title: "Senior Software Engineer",
    company_name: "Mira Software Security",
    icon: mira,
    iconBg: "#383E56",
    date: "Dec 2019 - Oct 2021",
    points: [
      "Lead engineer and product owner of product quality lifecycle management orchestrator.",
      "Improving product test coverage by implementing unit, functional, and performance tests.",
      "Implement methods to deploy (baremetal and virtual) SSL encryption/decryption appliances successfully, predictably, and reliably.",
      "Performed rigorous analysis and delivered key insights into TCP, TLS 1.2, TLS 1.3, HTTP/1, and HTTP/2.",
      "Lead backend developer of a highly-distributed and scalable central manager system with high transaction rates.",
      "Collaborating with cross-functional teams including designers, product managers, and other developers to create high-quality products.",
      "Implementing responsive design and ensuring cross-browser compatibility.",
      "Participating in code reviews and providing constructive feedback to other developers.",
    ],
  },
  {
    title: "Senior Data and Software Engineer",
    company_name: "Knowledgemarker",
    icon: km,
    iconBg: "#E6DEDD",
    date: "Nov 2021 - Apr 2022",
    points: [
      "Led a project to improve scalability and efficiency, while optimizing the earning potential of the platform.",
      "Identified and strategized data-scientific ways to accurately characterise the technical skills of our users; A key driver for revenue growth.",
      "Designed, developed, and maintained high-transaction API endpoints.",
      "Established unified short- and long-term vision of the platform.",
      "Established internal company processes.",
      "Established and maintained company vision and strategy.",
      "Owned the platform backend.",
      "Led a team of cross-functional engineers.",
      "Made critical contributions to benefit the business and marketing strategy.",
      "Mentored team members and reviewed code.",
    ],
  },
  {
    title: "Senior Software Engineer",
    company_name: "Byte Orbit",
    icon: bo,
    iconBg: "#E6DEDD",
    date: "Apr 2022 - Present",
    points: [
      "Design and deliver software components on the backend of a high-transaction global FinTech solution.",
      "Work closely with customers, partners, and project owners to gather requirements.",
      "Translate business requirements into technical design.",
      "Coordinate development efforts between developers, testers, and managers and define sprint goals.",
      "Conduct code reviews.",
      "Practice clean code principles and test driven development.",
      "Maintain and enhance current solutions and business processes, while ensuring greater efficiency, better documentation, and simplicity.",
      "Own the platform backend of a global and multi-award winning innovative fintech app.",
      "System design and architecture for enhancing the company's flagship project.",
      "Containerising and transitioning the project to a micro-services architecture.",
      "Company New Ventures Department: Senior consultant in the area of drone technology and computational intelligence."
    ],
  },
];

const testimonials = [
  {
    testimonial:
      "As an instructor and supervisor of Dr. E. America, I highly recommend him for research and problem-solving tasks. He was one of the top 5% of my students, and his exceptional performance continued through his postgraduate research. Ezra is an independent thinker and engages peers and supervisors when necessary. He demonstrates ambitious thinking and balance between theoretical rigor and practical execution skills. He is ideal for generating research ideas and leading investigations with high academic and social impact.",
    name: "Prof. Samuel Tsoeu",
    designation: "Head of Artificial Intelligence, Instrumentation and Control",
    company: "University of Cape Town",
    image: sammy,
  },
  {
    testimonial:
      "I taught Ezra control engineering and later supervised him as a teaching assistant during his postgraduate studies. His excellent academic record and ability to manage course tutors and support diverse students demonstrated his intellect and curiosity. Ezra worked well in a team and effectively collaborated with course instructors to manage a large class of undergraduate students.",
    name: "Prof. Ed Boje",
    designation: "Department Head of Engineering",
    company: "University of Cape Town",
    image: boje,
  },
  {
    testimonial:
      "I've worked with Ezra for several years. During this time, Ezra has always made it his responsibility to innovate his space. I've not come across a problem that he was not willing to tackle. He, without a doubt, would be a key contributor wherever he goes.",
    name: "Rando Wiesemann",
    designation: "Director of Engineering",
    company: "Netronome and Mira Software Security",
    image: rando,
  },
  {
    testimonial:
      "As the Chief Architect, I must say that Ezra has truly impressed me with his ownership of the performance and product quality space across the company. He displayed his exceptional talent in leading the backend development of our pioneering solution for globally distributed networks, and his ability to identify and implement initiatives to improve our lab infrastructure was remarkable. Ezra is an excellent leader with strong leadership attributes, and his contributions have been invaluable to our team's success.",
    name: "Johann Tonsing",
    designation: "Chief Architect",
    company: "Netronome and Mira Software Security",
    image: jt,
  },
  {
    testimonial:
      "During my experience working with Ezra, I have observed that he consistently delivers exceptional work in software and product design, while maintaining high security standards and offering valuable recommendations for improving code. He has exhibited strong leadership qualities in owning the platform stream of a successful fintech app, and is focused on personal and professional growth for himself and his team.",
    name: "Carel Burger",
    designation: "CTO",
    company: "Byte Orbit",
    image: charl,
  },
  {
    testimonial:
      "Ezra has been instrumental in helping to get the business off the ground. He showed extreme drive and resourcefulness to ensure our products are successful. He also identified significant gaps in our solutions, and made it his priority to find great future-proof solutions.",
    name: "Ryan Reed",
    designation: "CEO",
    company: "Knowledgemarker",
    image: ryan,
  },
  {
    testimonial:
      "Ezra is extremely inquisitive. He always finds problems that had gone unnoticed, and takes the responsibility upon himself to find and implement a MVP solution. He always cares about the team, and provides expert advice and guidance.",
    name: "Nolin Reddy",
    designation: "CTO",
    company: "Knowledgemarker",
    image: nolan,
  },
];

const projects = [
  {
    name: "Wideband characterization and image reconstruction of malignant tissue",
    description:
      "This project forms part of my doctoral research. Essentially, my work is the first successful method to completely characterize the frequency behaviour of biological (malignant or benign) tissue, using Electrical Impedance Tomography and Spectroscopy. Many of the tools used were mathematical, statistical, and scientific. These included methods of regularization, linear and vector algebra, iteration, optimization, control theory, image and signal processing, signal synthesis, signal generation, filtering, orthogonalization, finite element modelling, etc.",
    tags: [
      {
        name: "Python, C, C#, VHDL, Verilog",
        color: "blue-text-gradient",
      },
      {
        name: "MATLAB and Simulink",
        color: "green-text-gradient",
      },
      {
        name: "Quartus, FPGA, Git",
        color: "pink-text-gradient",
      },
    ],
    image: eit,
    source_code_link: "",
  },
  {
    name: "taskIT",
    description:
      "Taskit is a test lifecycle management and orchestrator service developed to automate testing and help Mira Security cope with a rapid decline in staff members. The service includes features such as model creation, scheduling, system health management, real-time logging, seamless test importing, and reporting. The service is fully adaptable and smart, utilizing optimized parallel computing and decentralized execution techniques to ensure scalability, efficiency, and offloading of compute-intensive tasks. The outcome of Taskit is that more time is utilized for developing more tests to more significantly test the products, while testing and reporting are completely automated.",
    tags: [
      {
        name: "Django, Python, HTML5, CSS3, Sass, Javascript, Typescript, Robot Framework, XML, Cypress, Bash, Tmux, GSpread, Bravado, VIM",
        color: "blue-text-gradient",
      },
      {
        name: "Bootstrap, jQuery, Ajax, Celery, Numpy, Pandas, Selenium, Cryptography, Cronjob, OpenSSL",
        color: "green-text-gradient",
      },
      {
        name: "Redis, PostgreSQL, VMWare, rabbitMQ, Nginx, WSGI, MySQL, Virsh, Visual Studio, Linux",
        color: "pink-text-gradient",
      },
    ],
    image: taskit,
    source_code_link: "",
  },
  {
    name: "Autonomous Investing",
    description:
    "This project involved developing a property investment management software. It has been my main source of finding, analyzing, and acquiring above-average performing investments. Some features include: Market analysis, economic analysis, property investment analysis, investment projections, forecasting cashflows, quantitative analysis, financial reporting and analysis, discount cashflow modelling, investor report generation, repo rate estimation, utilizing several common modelling techniques, Monte Carlo simulations, computing the Sharpe ratio, risk analysis, estimating the degree of operating and investment leverage, etc.",
    tags: [
      {
        name: "Python, Golang, Ansible, Elasticsearch, Bash",
        color: "blue-text-gradient",
      },
      {
        name: "JSON, YAML, Git, Gerrit, Docker, Kubernetes",
        color: "green-text-gradient",
      },
      {
        name: "Windows, Linux, iOS, VMWare",
        color: "pink-text-gradient",
      },
    ],
    image: invest,
    source_code_link: "",
  },
  {
    name: "Shyft",
    description:
      "Shyft is a money app that gives you more for less, 24/7. Buy Forex instantly anytime, anywhere, and at the best rates, and invest in the top U.S. stocks and ETFs directly from your phone. I led the platform team for this project.",
    tags: [
      {
        name: "Django, AngularJS, ReactJS",
        color: "blue-text-gradient",
      },
      {
        name: "AWS, Docker, Kubernetes",
        color: "green-text-gradient",
      },
      {
        name: "PostgreSQL, Redis, Cron",
        color: "pink-text-gradient",
      },
    ],
    image: shyft,
    source_code_link: "",
  },
  {
    name: "DroneTech",
    description:
      "This project dealt with the design and development of low-cost and efficient drone technology with extremely low latency data feeds. These drones house computer vision technology with cloud compute instances to detect and mitigate points of risk.",
    tags: [
      {
        name: "C, Python, Java, Ansible",
        color: "blue-text-gradient",
      },
      {
        name: "P2P, Docker",
        color: "green-text-gradient",
      },
      {
        name: "PostgreSQL, Redis, Cron",
        color: "pink-text-gradient",
      },
    ],
    image: drone,
    source_code_link: "",
  },
];

export { services, technologies, experiences, testimonials, projects };
